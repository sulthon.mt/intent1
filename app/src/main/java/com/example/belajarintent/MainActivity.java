package com.example.belajarintent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button explicit;
    Button implicit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        explicit = findViewById(R.id.explicit);
        explicit.setOnClickListener(this);
        implicit = findViewById(R.id.implicit);
        implicit.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.explicit:
                Intent explicit = new Intent(MainActivity.this, activity_intent.class);
                startActivity(explicit);
                break;
            case R.id.implicit:
                Intent implicit = new Intent(MainActivity.this, activity_intent.class);
                startActivity(implicit);
                break;
             default:
                 break;
        }
    }
}
